/* CAN Bus ECU Simulator
 *  
 * www.skpang.co.uk
 *
 * 
 */
 #include "ILI9341_t3.h"
 #define MENU_OFFSET   50
#define LINE_SPACING  15
#define VALUE_OFFSET  110
#define FONT_SIZE     11
#define CURSOR_OFFSET 46
#define CURSOR_LEN    12
#define L0_MAIN_MENU   0
#define L0_RPM         1
#define L0_COOLANT     2
#define L0_THROTTLE    3
#define L0_SPEED       4
#define L0_MAF         5
#define L0_O2          6
#define L0_DTC         7
#define JOG_INC        0
#define JOG_DEC        1
#include "ecu_sim.h"
#include <FlexCAN.h>
void blinkline(void);
void printmsg(char * str,int linenumber);
void clearmsg(void);
/* C++ wrapper */
ecu_simClass::ecu_simClass() {
 
}
void printframe(char * status,CAN_message_t msg);
extern ILI9341_t3 tft;
uint8_t ecu_simClass::init(uint32_t baud) {

  Can0.begin(baud);
  ecu.dtc = 0;
  return 0;
}

void ecu_simClass::dummywrite(void)
{
  CAN_message_t can_MsgTx;
  can_MsgTx.ext =0 ;
  can_MsgTx.rtr =0 ;
  can_MsgTx.buf[0] = 0x00; 
  can_MsgTx.buf[1] = 0x01;    
  can_MsgTx.buf[2] = 0x02;  
  can_MsgTx.buf[3] = 0x03;  
  can_MsgTx.buf[4] = 0x04;                
  can_MsgTx.buf[5] = 0x05;
  can_MsgTx.buf[6] = 0x06;
  can_MsgTx.buf[7] = 0x07;
  can_MsgTx.id = PID_REPLY;
  can_MsgTx.len = 8; 
  can_MsgTx.timeout= 500; //in milisecons, zero means no waiting
  Can0.write(can_MsgTx);
  clearmsg();
  delay(200);
  printframe("sent",can_MsgTx);
}
void ecu_simClass::periodicsend(void)
{ 
  CAN_message_t can_MsgTx;
  can_MsgTx.ext =1 ;
  can_MsgTx.rtr =0 ;
  can_MsgTx.id = PID_REPLY;
  can_MsgTx.id = 0x12345678;
  can_MsgTx.len = 8; 
  can_MsgTx.buf[1] = MODE1_RESPONSE;
  can_MsgTx.buf[0] = 0x04;  
  can_MsgTx.buf[2] = ENGINE_RPM; 
  can_MsgTx.buf[3] = (ecu.engine_rpm & 0xff00) >> 8;
  can_MsgTx.buf[4] = ecu.engine_rpm & 0x00ff;
  can_MsgTx.buf[5] = 0x11;
  can_MsgTx.buf[6] = 0x00;
  can_MsgTx.buf[7] = 0x00;
  Can0.write(can_MsgTx);
  printframe("sent",can_MsgTx);
}
void printframe(char * status,CAN_message_t msg)
{
        tft.setCursor(10, MENU_OFFSET + (LINE_SPACING * (L0_DTC+1)));  
  tft.fillRect(10,MENU_OFFSET + (LINE_SPACING * (L0_DTC+1)),340,60 ,ILI9341_BLACK); // Clear area
  tft.print("Status : ");tft.print(status);
  tft.setCursor(10, MENU_OFFSET + (LINE_SPACING * (L0_DTC+2))); 
  uint8_t * p;
  p=(uint8_t *)&msg.id;
  tft.print("Msg_Id : ");tft.print(p[0]);tft.print(" ");tft.print(p[1]);tft.print(" ");tft.print(p[2]);tft.print(" ");tft.print(p[3]);
  tft.setCursor(10, MENU_OFFSET + (LINE_SPACING * (L0_DTC+3))); 
  tft.print("Data   : ");
  for(int i=0;i<msg.len;i++){
    tft.print(" ");tft.print(msg.buf[i]);
  }
}
uint8_t ecu_simClass::update(void) 
{
  CAN_message_t can_MsgRx,can_MsgTx;

//  can_MsgTx.flags.extended = 0; 
//  can_MsgTx.flags.remote = 0; 
  can_MsgTx.ext =1 ;
  can_MsgTx.rtr =0 ;
  if(Can0.read(can_MsgRx)) 
  {
    blinkline();
    clearmsg();
    delay(200);
    printframe("received",can_MsgRx);
    
     if (can_MsgRx.id == PID_REQUEST) 
     {
        can_MsgTx.ext = 0; 
        if(can_MsgRx.buf[1] == MODE3) // Request trouble codes
        {
            if(ecu.dtc == false){
                can_MsgTx.buf[0] = 0x02; 
                can_MsgTx.buf[1] = MODE3_RESPONSE;    
                can_MsgTx.buf[2] = 0x00;  
             }else{
                can_MsgTx.buf[0] = 0x06; 
                can_MsgTx.buf[1] = MODE3_RESPONSE;    
                can_MsgTx.buf[2] = 0x02;  
                can_MsgTx.buf[3] = 0x01;  
                can_MsgTx.buf[4] = 0x00;                
                can_MsgTx.buf[5] = 0x02;
                can_MsgTx.buf[6] = 0x00;                
             }
             can_MsgTx.id = PID_REPLY;
             can_MsgTx.len = 8; 
             Can0.write(can_MsgTx);
        }
      
        if(can_MsgRx.buf[1] == MODE4) // Clear trouble codes, clear Check engine light
        {
            ecu.dtc = false;  
      
            can_MsgTx.buf[0] = 0x00; 
            can_MsgTx.buf[1] = MODE4_RESPONSE; 
            can_MsgTx.id = PID_REPLY;
            can_MsgTx.len = 8; 
            Can0.write(can_MsgTx);  
        }
        
        if(can_MsgRx.buf[1] == MODE1)
        {
            can_MsgTx.id = PID_REPLY;
            can_MsgTx.len = 8; 
            can_MsgTx.buf[1] = MODE1_RESPONSE;
            
            switch(can_MsgRx.buf[2])
            {   /* Details from http://en.wikipedia.org/wiki/OBD-II_PIDs */
                case PID_SUPPORTED:
                    can_MsgTx.buf[0] = 0x06;  
                    can_MsgTx.buf[2] = PID_SUPPORTED; 
                    can_MsgTx.buf[3] = 0xE8;
                    can_MsgTx.buf[4] = 0x19;
                    can_MsgTx.buf[5] = 0x30;
                    can_MsgTx.buf[6] = 0x12;
                    can_MsgTx.buf[5] = 0x00;
                    Can0.write(can_MsgTx);  
                    break;
                
                case MONITOR_STATUS:
                    can_MsgTx.buf[0] = 0x05;  
                    can_MsgTx.buf[2] = MONITOR_STATUS; 
                    if(ecu.dtc == true) can_MsgTx.buf[3] = 0x82;
                        else can_MsgTx.buf[3] = 0x00;
                    
                    can_MsgTx.buf[4] = 0x07;
                    can_MsgTx.buf[5] = 0xFF;
                    Can0.write(can_MsgTx);      
                    break;
                        
                case ENGINE_RPM:              //   ((A*256)+B)/4    [RPM]
                    can_MsgTx.buf[0] = 0x04;  
                    can_MsgTx.buf[2] = ENGINE_RPM; 
                    can_MsgTx.buf[3] = (ecu.engine_rpm & 0xff00) >> 8;
                    can_MsgTx.buf[4] = ecu.engine_rpm & 0x00ff;
                    Can0.write(can_MsgTx);
                    break;
                               
                case ENGINE_COOLANT_TEMP:     //     A-40              [degree C]
                    can_MsgTx.buf[0] = 0x03;  
                    can_MsgTx.buf[2] = ENGINE_COOLANT_TEMP; 
                    can_MsgTx.buf[3] = ecu.coolant_temp;
                    Can0.write(can_MsgTx);
                    break;
                               
                case VEHICLE_SPEED:         // A                  [km]
                    can_MsgTx.buf[0] = 0x03;  
                    can_MsgTx.buf[2] = VEHICLE_SPEED; 
                    can_MsgTx.buf[3] = ecu.vehicle_speed;
                    Can0.write(can_MsgTx);
                    break;
    
                case MAF_SENSOR:               // ((256*A)+B) / 100  [g/s]
                    can_MsgTx.buf[0] = 0x04;  
                    can_MsgTx.buf[2] = MAF_SENSOR; 
                    can_MsgTx.buf[3] = (ecu.maf_airflow & 0xff00) >> 8;
                    can_MsgTx.buf[4] =  ecu.maf_airflow & 0x00ff;
                    Can0.write(can_MsgTx);
                    break;
    
                case O2_VOLTAGE:            // A * 0.005   (B-128) * 100/128 (if B==0xFF, sensor is not used in trim calc)
                    can_MsgTx.buf[0] = 0x04;  
                    can_MsgTx.buf[2] = O2_VOLTAGE; 
                    can_MsgTx.buf[3] = ecu.o2_voltage & 0x00ff;
                    can_MsgTx.buf[4] = (ecu.o2_voltage & 0xff00) >> 8;
                    Can0.write(can_MsgTx);
                    break;;
                   
                case THROTTLE:            //
                    can_MsgTx.buf[0] = 0x03;  
                    can_MsgTx.buf[2] = THROTTLE; 
                    can_MsgTx.buf[3] = ecu.throttle_position;
                    Can0.write(can_MsgTx);
                    break;
              }//switch
          }
       }
    }
   return 0;
}
     
ecu_simClass ecu_sim;
